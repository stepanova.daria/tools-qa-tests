import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SliderPage {
    private WebDriver driver;

    public SliderPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = "//input[@class='range-slider range-slider--primary']")
    private WebElement slider;

    @FindBy(xpath = "//input[@id='sliderValue']")
    private WebElement sliderValueInput;

    public void moveSliderRight(int nOfSteps) {
        for (int i = 1; i <= nOfSteps; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
    }

    public void moveSliderLeft(int nOfSteps) {
        for (int i = 1; i <= nOfSteps; i++) {
            slider.sendKeys(Keys.ARROW_LEFT);
        }
    }

    public int getSliderInputValue() {
        String valueStr = sliderValueInput.getAttribute("value");
        int sliderInputValue = Integer.parseInt(valueStr);
        return sliderInputValue;
    }

}
