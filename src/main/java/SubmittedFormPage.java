import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SubmittedFormPage {
    private WebDriver driver;

    public SubmittedFormPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = "//*[@id='example-modal-sizes-title-lg']")
    private WebElement successMessage;

    @FindBy(xpath = "//table[@class='table table-dark table-striped table-bordered table-hover']//tbody/tr/td[2]")
    private WebElement studentName;

    @FindBy(xpath = "//table[@class='table table-dark table-striped table-bordered table-hover']//tbody/tr[2]/td[2]")
    private WebElement studentEmail;

    @FindBy(xpath = "//table[@class='table table-dark table-striped table-bordered table-hover']//tbody/tr[4]/td[2]")
    private WebElement mobile;

    @FindBy(xpath = "//table[@class='table table-dark table-striped table-bordered table-hover']//tbody/tr[6]/td[2]")
    private WebElement subject;

    @FindBy(xpath = "//table[@class='table table-dark table-striped table-bordered table-hover']//tbody/tr[8]/td[2]")
    private WebElement picture;

    @FindBy(xpath = "//table[@class='table table-dark table-striped table-bordered table-hover']//tbody/tr[9]/td[2]")
    private WebElement address;

    @FindBy(xpath = "//table[@class='table table-dark table-striped table-bordered table-hover']//tbody/tr[10]/td[2]")
    private WebElement cityAndState;

    public boolean isSuccessMessageDisplayed() {
        return successMessage.isDisplayed();
    }

    public String getName() {
        return studentName.getText();
    }

    public String getEmail() {
        return studentEmail.getText();
    }

    public String getNumber() {
        return mobile.getText();
    }

    public String getSubject() {
        return subject.getText();
    }

    public String getPictureName() {
        return picture.getText();
    }

    public String getAddress() {
        return address.getText();
    }

    public String getCityAndState() {
        return cityAndState.getText();
    }

}
