import org.apache.commons.compress.utils.FileNameUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.IOException;

public class PracticeFormPage {

    private WebDriver driver;

    public PracticeFormPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = "//input[@id='firstName']")
    private WebElement firstNameField;
    @FindBy(xpath = "//input[@id='lastName']")
    private WebElement lastNameField;
    @FindBy(xpath = "//input[@id='userEmail']")
    private WebElement emailField;
    @FindBy(xpath = "//label[@for='gender-radio-1']")
    private WebElement gender;
    @FindBy(xpath = "//input[@id='dateOfBirthInput']")
    private WebElement dateOfBirthInput;
    @FindBy(xpath = "//input[@id='userNumber']")
    private WebElement numberField;
    @FindBy(xpath = "//*[@id='subjectsInput']")
    private WebElement subjectsField;
    @FindBy(xpath = "//label[@for='hobbies-checkbox-1']")
    private WebElement hobbies;
    @FindBy(xpath = "//*[@id='currentAddress']")
    private WebElement addressField;
    @FindBy(xpath = "//input[@id='react-select-3-input']")
    private WebElement stateField;
    @FindBy(xpath = "//input[@id='react-select-4-input']")
    private WebElement cityField;
    @FindBy(xpath = "//button[@id='submit']")
    private WebElement submit;
    @FindBy(xpath = "//input[@id='uploadPicture']")
    private WebElement upload;


    public PracticeFormPage fillForm(String firstName, String lastName, String email, String number, String subject, String address, String state, String city) {
        firstNameField.sendKeys(firstName);
        lastNameField.sendKeys(lastName);
        emailField.sendKeys(email);
        gender.click();
        numberField.sendKeys(number);
        selectValue(subjectsField, subject);
        hobbies.click();
        addressField.sendKeys(address);
        selectValue(stateField, state);
        selectValue(cityField, city);
        return new PracticeFormPage(driver);
    }

    public PracticeFormPage uploadFile(File template, File copy) {
        upload.sendKeys(copy.getAbsolutePath());
        copy.deleteOnExit();
        return new PracticeFormPage(driver);
    }

    public PracticeFormPage submitData() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", submit);
        return new PracticeFormPage(driver);
    }

    public void selectValue(WebElement element, String value) {
        element.sendKeys(value);
        element.sendKeys(Keys.ARROW_DOWN);
        element.sendKeys(Keys.ENTER);
    }

    public static File createDuplicate(File template) {
        File parent = template.getParentFile();
        String name = template.getName();
        String baseName = FileNameUtils.getBaseName(name);
        String extension = FileNameUtils.getExtension(name);
        String nameOfCopy = baseName + "_" + RandomStringUtils.random(6) + "." + extension;
        File copy = new File(parent, nameOfCopy);
        try {
            FileUtils.copyFile(template, copy);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return copy;
    }

}
