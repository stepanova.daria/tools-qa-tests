import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;

public class ToolsQaTests extends BaseTestClass {

    String fistName = "Something";
    String lastName = "New";
    String email = "test@gmail.com";
    String number = "6104859031";
    String subject = "English";
    String address = "5 Elm Street";
    String city = "Uttar Pradesh";
    String state = "Agra";

    @Test
    public void formTest() {
        File template = new File("src/test/resources/files/testpic.png");
        File copy = new PracticeFormPage(driver).createDuplicate(template);
        String uploadedFileName = copy.getName();
        goToUrl("https://demoqa.com/automation-practice-form");
        new PracticeFormPage(driver)
                .fillForm(fistName, lastName, email, number, subject, address, city, state)
                .uploadFile(template, copy)
                .submitData();
        Assert.assertTrue(new SubmittedFormPage(driver).isSuccessMessageDisplayed());
        Assert.assertEquals(new SubmittedFormPage(driver).getName(), fistName + " " + lastName);
        Assert.assertEquals(new SubmittedFormPage(driver).getEmail(), email);
        Assert.assertEquals(new SubmittedFormPage(driver).getNumber(), number);
        Assert.assertEquals(new SubmittedFormPage(driver).getSubject(), subject);
        Assert.assertEquals(new SubmittedFormPage(driver).getAddress(), address);
        Assert.assertEquals(new SubmittedFormPage(driver).getCityAndState(), city + " " + state);
        Assert.assertEquals(new SubmittedFormPage(driver).getPictureName(), uploadedFileName);
    }

    @Test
    public void browserTabTest() {
        goToUrl("https://demoqa.com/browser-windows");
        driver.findElement(By.xpath("//button[@id='tabButton']")).click();
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id='sampleHeading']")).getText(), "This is a sample page");
        driver.switchTo().window(tabs.get(0));
        Assert.assertEquals(driver.findElement(By.xpath("//div[@class='main-header']")).getText(), "Browser Windows");
    }

    @Test
    public void sliderTest() {
        goToUrl("https://demoqa.com/slider");
        int initValue = new SliderPage(driver).getSliderInputValue();
        new SliderPage(driver).moveSliderLeft(3);
        Assert.assertEquals(new SliderPage(driver).getSliderInputValue(), initValue - 3);
        int valueAftMoving = new SliderPage(driver).getSliderInputValue();
        new SliderPage(driver).moveSliderRight(2);
        Assert.assertEquals(new SliderPage(driver).getSliderInputValue(), valueAftMoving + 2);
    }

}
